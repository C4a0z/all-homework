package Task_6;

import java.time.LocalTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestSynchronize {


    public static void main(String[] args) {
        Runnable firstThread = () -> {
            callingMethod();
            System.out.println("1 поток доступился");
        };

        Runnable secondThread = () -> {
            callingMethod2();
            System.out.println("2 поток доступился");
        };
        Runnable thirdThread = () -> {
            callingMethod3();
            System.out.println("3 поток доступился");
        };
        ExecutorService executors = Executors.newFixedThreadPool(3);
        executors.submit(firstThread);
        executors.submit(secondThread);
        executors.submit(thirdThread);
        executors.shutdown();
    }


   static synchronized void callingMethod() {
        System.out.println(Thread.currentThread().getName() + " вызвал метод синхронизации " + LocalTime.now());
    }
    static synchronized void callingMethod2() {
        System.out.println(Thread.currentThread().getName() + " вызвал метод синхронизации " + LocalTime.now());
    }
    static synchronized void callingMethod3() {
        System.out.println(Thread.currentThread().getName() + " вызвал метод синхронизации " + LocalTime.now());
    }

}
