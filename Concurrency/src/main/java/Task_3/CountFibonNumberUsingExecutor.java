package Task_3;

import java.time.Duration;
import java.time.LocalTime;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

class CountFibonNumberUsingExecutor {


    public CountFibonNumberUsingExecutor(int requiredNumber) {
        this.requiredNumber = requiredNumber;
    }

    public static void main(String[] args) throws InterruptedException {



        CountFibonNumberUsingExecutor countFibonNumber = new CountFibonNumberUsingExecutor(15);

        LocalTime time = LocalTime.now();

        System.out.println(countFibonNumber.makingCounting());

        LocalTime time1 = LocalTime.now();

        Duration duration = Duration.between(time, time1);

        System.out.println("Time: " + duration.toMillis());


    }


    private int currentPosition = 2;
    private int requiredNumber;
    private int currentNumber = 1;
    private int previousNumber = 0;
    private int requiredFibonacciNumber;
    private static Object sync = new Object();


    private synchronized Integer makingCounting() throws InterruptedException {



        Thread t1 = new Thread(() -> {
            synchronized (sync) {
                for (; currentPosition <= requiredNumber; currentPosition++) {
                    try {
                        sync.wait();
                    } catch (Exception e) {
                    }
                    if (currentPosition <= requiredNumber) {
                        requiredFibonacciNumber = currentNumber + previousNumber;
                        previousNumber = currentNumber;
                        currentNumber = requiredFibonacciNumber;
                        System.out.println(currentNumber);
                        sync.notify();
                    } else {
                        break;
                    }

                }
            }
        });

        Thread t2 = new Thread(() -> {
            synchronized (sync) {
                for (; currentPosition <= requiredNumber; currentPosition++) {
                    sync.notify();
                    try {
                        sync.wait();
                    } catch (Exception e) {
                    }
                    if (currentPosition <= requiredNumber) {
                        requiredFibonacciNumber = currentNumber + previousNumber;
                        previousNumber = currentNumber;
                        currentNumber = requiredFibonacciNumber;
                        System.out.println(currentNumber);
                    } else {
                       break;
                    }

                }
            }
        });


        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.submit(t1);
        executorService.submit(t2);
        executorService.shutdownNow();
        executorService.awaitTermination(5,TimeUnit.MICROSECONDS);
        System.out.println(executorService.isShutdown());
        System.out.println(executorService.isTerminated());
        return requiredFibonacciNumber;
    }


}



