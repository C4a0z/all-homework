package Task_1;

import java.util.Scanner;

public class Rectangle {

    static private Scanner scanner = new Scanner(System.in);


    public static void main(String[] args) {
        Integer a = null;
        Integer b = null;
        do {
            System.out.println("Please enter first size");
            Integer localVariableA = scanner.nextInt();
            try {
                if (localVariableA < 0) {
                    throw new NegativeNumbersEcxeption();
                } else a = localVariableA;
            } catch (NegativeNumbersEcxeption e) {
                System.out.println("Please enter positive number");
            }
        } while (a == null);
        do {
            System.out.println("Please enter second size");
            Integer localVariableB = scanner.nextInt();
            try {
                if (localVariableB < 0) {
                    throw new NegativeNumbersEcxeption();
                } else b = localVariableB;
            } catch (NegativeNumbersEcxeption e) {
                System.out.println("Please enter positive number");
            }
        } while (b == null);
        System.out.println(

                squareRectangle(a, b));

    }


    static int squareRectangle(int a, int b) {
        int area = a * b;
        return area;

    }
}
