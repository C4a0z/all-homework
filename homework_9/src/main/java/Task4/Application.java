package Task4;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Application {
    private Scanner scanner = new Scanner(System.in);
    private List<String> listOfLines = new ArrayList<>();
    private int numberOfUniqueWords = 0;
    private List<String> sortedUniqueList;

    public Application() {
        addingLinesToList();
    }

    public static void main(String[] args) {
        Application application = new Application();
        application.countingNumberOfUniqueWords();
    }

    void countingNumberOfUniqueWords() {
        numberOfUniqueWords = (int) listOfLines.stream()
                .distinct()
                .count();
    }

    void sortedListWithUniqueWords() {
        sortedUniqueList = listOfLines.stream()
                .distinct()
                .sorted()
                .collect(Collectors.toList());
        getSortedUniqueList();
    }

    void addingLinesToList() {
        while (true) {
            String line = scanner.nextLine();
            if (!line.equals("")) {
                listOfLines.add(line);
            } else {
                break;
            }
        }
    }

    public List<String> getSortedUniqueList() {
        return sortedUniqueList;
    }
}
