package Task_5;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Exersice {
    public static void main(String[] args) throws Exception {
        String line = null;
        Scanner scanner = new Scanner(new InputStreamReader(
                new BufferedInputStream(
                        new FileInputStream("JavaClass.java"))));
        while (scanner.hasNext()) {

            line = scanner.nextLine();

            int index = line.indexOf("//");
            if (index != -1) {
                System.out.println(line.substring(index));
            }
        }
    }
}
