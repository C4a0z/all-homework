import annotation.Printable;
import annotation.PrintableMethod;

import java.lang.reflect.Field;

public class Book {

    @Printable(nameOfVariable = "AnnotationBook")
    public String name = "Some book";


    public String someSentence = "Sentence1";


    @Printable(nameOfVariable = "AnnotationSentence")
    public String SomeSentence = "Sentence1";

    public String someField;






    @PrintableMethod("Concat name and 1Sentence")
   public String concatNameAndFirstSentence() {

        return name + someSentence;
    }
    @PrintableMethod("NumberOfFields")
   public int numberOfFields() {
        Field[] fields = Book.class.getFields();

        return fields.length-1;
    }


}
