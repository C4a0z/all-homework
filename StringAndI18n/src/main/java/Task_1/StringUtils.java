package Task_1;

import java.util.ArrayList;
import java.util.List;

public class StringUtils {
    List<Object> listOfObject = new ArrayList<>();


    public StringUtils addNewParameter(Object obj) {
        listOfObject.add(obj);
        return this;
    }

    public String concat() {
        StringBuilder stringBuilder = new StringBuilder();

        for (Object obj : listOfObject) {
            stringBuilder.append(obj + "  ");
        }
        return stringBuilder.toString();
    }
}