import annotation.Printable;
import annotation.PrintableMethod;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Reader {
    public static void main(String[] args) {


        printingFieldsWithAnn(Book.class);
        printingAnnotationValue(Book.class);
        printingMethodType(Book.class);

    }


    static void printingFieldsWithAnn(Class<?> obj) {

        Field[] fields;
        fields = obj.getFields();
        System.out.println("Field With Annotation\n");
        Arrays.stream(fields)
                .filter(a -> a.isAnnotationPresent(Printable.class))
                .forEach(field -> System.out.println(field.getName()));

    }

    static void printingAnnotationValue(Class<?> obj) {
        Field[] fields;
        fields = obj.getFields();
        System.out.println("\nAnnotation value\n" +
                "");
        Arrays.stream(fields)
                .filter(a -> a.isAnnotationPresent(Printable.class))
                .forEach(field -> System.out.println(field.getAnnotation(Printable.class)
                        .nameOfVariable()));
    }

    static void printingMethodType(Class<?> obj) {

        Method[] methods;
        methods = obj.getMethods();


        Arrays.stream(methods)
                .filter((Method method) -> method.isAnnotationPresent(PrintableMethod.class))
                .forEach(method -> System.out.println(method.getReturnType()));

    }


}
