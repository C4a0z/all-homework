import java.io.Serializable;

public class Gem implements Serializable {
    private int gemNo;
    private String name;
    private String preciousness;
    private String origin;
    private VisualParameters visualParameters;
    private float weight;
    private String treatment;

    public Gem(int gemNo, String name, String preciousness,
               String origin, VisualParameters visualParameters,
               float weight, String treatment) {
        this.gemNo = gemNo;
        this.name = name;
        this.preciousness = preciousness;
        this.origin = origin;
        this.visualParameters = visualParameters;
        this.weight = weight;
        this.treatment = treatment;
    }

    public int getGemNo() {
        return gemNo;
    }

    public void setGemNo(int gemNo) {
        this.gemNo = gemNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPreciousness() {
        return preciousness;
    }

    public void setPreciousness(String preciousness) {
        this.preciousness = preciousness;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public String getTreatment() {
        return treatment;
    }

    public void setTreatment(String treatment) {
        this.treatment = treatment;
    }

    @Override
    public String toString() {
        return "Gem{" +
                "gemNo=" + gemNo +
                ", name='" + name + '\'' +
                ", preciousness='" + preciousness + '\'' +
                ", origin='" + origin + '\'' +
                ", visualParameters=" + visualParameters +
                ", weight='" + weight + '\'' +
                ", treatment='" + treatment + '\'' +
                '}';
    }
}
