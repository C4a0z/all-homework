package Task_3;


import java.io.*;
import java.time.Duration;
import java.time.LocalTime;

public class WorkingWithData {

    public static void main(String[] args) {
        LocalTime pt1 = LocalTime.now();
        AirShip airShip = null;
        LocalTime pt2 = LocalTime.now();
       // Duration d1 = Duration.between(pt1, pt2);
     //  System.out.println("Creating time " + d1.toSeconds());

        WorkingWithData writingClass = new WorkingWithData();
        LocalTime lt = LocalTime.now();
       // writingClass.writeToFile(airShip);
        airShip = writingClass.readToObj();
        LocalTime lt1 = LocalTime.now();

        Duration duration = Duration.between(lt, lt1);


        System.out.println("Время выполнения: " + duration.toSeconds());


    }

    void writeToFile(AirShip object) {
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(new BufferedOutputStream(
                    new FileOutputStream("Test.dat")));
            out.writeObject(object);
        } catch (Exception e) {
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    AirShip readToObj() {
        ObjectInputStream ois;
        AirShip airShip = null;
        try {
            ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream("Test.dat")));
            airShip = (AirShip) ois.readObject();
            ois.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
        }
        return airShip;
    }
}
