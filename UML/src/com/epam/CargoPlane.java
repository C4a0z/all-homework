package com.epam;

public class CargoPlane extends AbstractPlane {
    private float cargo;

    public CargoPlane(String name, float volumeOfFuel, float fuelConsumptionPerKilometer, float cargo) {
        super(name, volumeOfFuel, fuelConsumptionPerKilometer);
        this.cargo=cargo;
    }

    public float getCargo() {
        return cargo;
    }

    @Override
    public String toString() {
        return "CargoPlane{" +
                "cargo=" + cargo +
                '}';
    }
}
