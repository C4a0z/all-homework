package Task_2;

public class Plants {
    private int size;
    private Color color;
    private Type type;


    public Plants(int size, Color color, Type type) {

        this.size = size;
        do {
            if ((color == Color.RED) || (color == Color.GREEN) || (color == Color.YELLOW) || (color == Color.WHITE)) {
                this.color = color;
            } else {
                try {
                    throw new ColorException();
                } catch (ColorException e) {
                    System.out.println("Вы ввели не правильный цвет");
                }
            }
        } while (
                this.color == null);

        do {
            if ((type == Type.TYPE1) || (type == Type.TYPE2) || (type == Type.TYPE3)) {
                this.type = type;
            } else {
                try {
                    throw new TypeException();
                } catch (TypeException e) {
                    System.out.println("Вы ввели не правильный тип растения");
                }
            }
        } while (
                this.color == null);

        this.type = type;
    }

    @Override
    public String toString() {
        return "Plants{" +
                "size=" + size +
                ", color=" + color +
                ", type=" + type +
                '}';
    }

    public static void main(String[] args) {
        Plants plants = new Plants(1, Color.YELLOW, Type.TYPE1);
        plants.toString();
    }

}
