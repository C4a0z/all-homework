package task2;

import java.util.Scanner;

public class Main implements Command {
    static Main mainClass = new Main();
    static Scanner scanningCommand = new Scanner(System.in);
    //не совсм понял задание, сделал как понял
    Command lambdaCommand = arg -> System.out.println(arg);

    Command objCommand = new Command() {
        @Override
        public void receivingStringArgument(String arg) {

        }
    };

    @Override
    public void receivingStringArgument(String arg) {
        System.out.println(arg);
    }

    public static void main(String[] args) {
        System.out.println("Enter command");
        System.out.println("1 - Lambda");
        System.out.println("2 - Object");
        System.out.println("3 - Method");
        switch (scanningCommand.next()) {
            case "Lambda": {
                System.out.println("Enter arg");
                mainClass.lambdaCommand.receivingStringArgument(scanningCommand.next());
                break;
            }
            case "Object": {
                System.out.println("Enter arg");
                mainClass.objCommand.receivingStringArgument(scanningCommand.next());
                break;
            }
            case "Method":{
                System.out.println("Enter arg");
                mainClass.receivingStringArgument(scanningCommand.next());
            }
        }
    }
}
