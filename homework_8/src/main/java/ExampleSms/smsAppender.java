package ExampleSms;

import java.io.Serializable;

import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.*;
import org.apache.logging.log4j.core.layout.PatternLayout;




@Plugin(name = "SMS", category = "Core", elementType = "appender", printObject = true)
public final class smsAppender extends AbstractAppender {
    public smsAppender(String name, Filter filter, Layout<? extends Serializable> layout, boolean ignoreExceptions) {
        super(name, filter, layout, ignoreExceptions);
    }

    public void append(LogEvent logEvent) {
        try {
            ExampleSMS.send(new String(getLayout().toByteArray(logEvent)));
        } catch (Exception ex) {
        }
    }

    @PluginFactory
    public static smsAppender createAppender(
            @PluginAttribute("name") String name,
            @PluginElement("Layout") Layout<? extends Serializable> layout,
            @PluginElement("Filter") final Filter filter,
            @PluginAttribute("otherAttribute") String otherAttribute) {
        if (name == null) {
            LOGGER.error("No name provided for MyCustomAppenderImpl");
            return null;
        }
        if (layout == null) {
            layout = PatternLayout.createDefaultLayout();
        }
        return new smsAppender(name, filter, layout, true);
    }

}
