package com.epam.logical_masive;

import java.util.Arrays;

public class ExerciseB {
    static int[] values = {1, 2, 3, 4, 5, 5, 5, 6, 6, 7};

    public static void main(String[] args) {
        removeDuplicates();
    }

    public static void removeDuplicates() {
        int countingDuplacates;
        for (int i : values) {
            countingDuplacates =0;
            for (int k = 0; k < values.length; k++){
                if(i==values[k]){
                    countingDuplacates++;
                    if(countingDuplacates>2){
                        remove(k);
                    }
                }
            }
        }
    }
    private static void remove(int index) {
        if (index >= 0 && index < values.length) {
            int[] copy = new int[values.length - 1];
            System.arraycopy(values, 0, copy, 0, index);
            System.arraycopy(values, index + 1, copy, index, values.length - index - 1);
            values = copy;
        }

    }
}
