package com.epam.logical_masive.Generic.shipwithdroid;

import java.util.ArrayList;
import java.util.List;

public class Ship<T extends Droid> {
    List<T> listOfDroid = new ArrayList<>();

    public T getDroid(int positionOfDroid) {
        return listOfDroid.get(positionOfDroid);
    }

    public void setDroid(T arg) {
        this.listOfDroid.add(arg);
    }
}
