package com.epam.logical_masive.ExcersiseD;

public class Room {
    TypeOfAction typeOfAction = new TypeOfAction();
    private int randomNumber = (int) (Math.random() * 10);
    private int numberOfRoom;

    public TypeOfAction getTypeOfAction() {
        return typeOfAction;
    }

    public int getNumberOfRoom() {
        return numberOfRoom;
    }

    public Room(int numberOfRoom) {
        choosingTypeOfAction();
        this.numberOfRoom = numberOfRoom;
    }

    public void choosingTypeOfAction() {
        if (randomNumber < 5) {
            makeArtifactAction();
        } else {
            makeMonsterAction();
        }
    }

    private void makeArtifactAction() {
        typeOfAction.setName("Artifact");
        typeOfAction.setWhatCanDoWithPower((int) (Math.random() * 81) + 10);
    }

    private void makeMonsterAction() {
        typeOfAction.setName("Monster");
        typeOfAction.setWhatCanDoWithPower(-1 * ((int) (Math.random() * 101) + 5));
    }

    @Override
    public String toString() {
        return "Room{" +
                "typeOfAction=" + typeOfAction +
                ", numberOfRoom=" + numberOfRoom +
                '}';
    }
}
