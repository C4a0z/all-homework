package com.epam;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public final class Company {
    private Scanner scanner = new Scanner(System.in);
    private List<AbstractPlane> allPlanes = new ArrayList<>();

    public Company() {
        addingPlaneToList();
    }

    private void addingPlaneToList() {
        System.out.println("How much plan do you want to add?");
        int numberOfPlanes = scanner.nextInt();
        for (int i = 1; i <= numberOfPlanes; i++) {
            if (Math.random() * 2 < 1) {
                allPlanes.add(new PassengerPlane("#" + i,
                        (int) (Math.random() * 600) + 150,
                        (int) (Math.random() * 10) + 4,
                        (int) (Math.random() * 50) + 10));
            } else {
                allPlanes.add(new CargoPlane("#" + i,
                        (int) (Math.random() * 600) + 150,
                        (int) (Math.random() * 20) + 5,
                        (int) (Math.random() * 150) + 50));
            }
        }
    }

    public int totalCapacity() {
        int totalCapacity = 0;
        for (AbstractPlane plane : allPlanes) {
            if (plane instanceof CargoPlane) {
                totalCapacity += ((CargoPlane) plane).getCargo();
            }
        }
        return totalCapacity;
    }

    public int totalNumberOfSeats() {
        int totalNumberOfSeats = 0;
        for (AbstractPlane plane : allPlanes) {
            if (plane instanceof PassengerPlane) {
                totalNumberOfSeats += ((PassengerPlane) plane)
                        .getNumberOfSeats();
            }
        }
        return totalNumberOfSeats;
    }

    public void sortingForDistanceOfFly() {
        List<AbstractPlane> sortPlane;
        System.out.println("Sort Ascending - 1");
        System.out.println("or descending - 2");
        if (scanner.nextInt() == 1) {
            sortPlane = allPlanes.stream()
                    .sorted(Comparator.comparing(AbstractPlane::getDistanceOfFly))
                    .collect(Collectors.toList());
        } else {
            sortPlane = allPlanes.stream().
                    sorted((AbstractPlane a, AbstractPlane b) -> b.getDistanceOfFly()
                            .compareTo(a.getDistanceOfFly()))
                    .collect(Collectors.toList());
        }
        sortPlane.forEach(System.out::println);
    }

    public void findingPlanesWithSomeConsumption() {

        System.out.println("Please enter lower limit and upper limit");
        int lowerLimit = scanner.nextInt();
        int upperLimit = scanner.nextInt();
        allPlanes.stream()
                .filter(a -> a.getFuelConsumptionPerKilometer() > lowerLimit
                        && a.getFuelConsumptionPerKilometer() < upperLimit)
                .forEach(System.out::println);

    }


}
