package com.epam.Enums;

public enum MenuItems {
    START("Start game"), SAVE("Save game"), LOAD("Load game"), SETTINGS("Settings"), EXIT("Exit to desktop");
    private String nameOfItems;

    MenuItems(String nameOfItems) {
        this.nameOfItems = nameOfItems;
    }

    public String getNameOfItems() {
        return nameOfItems;
    }
}
