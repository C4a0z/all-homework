package Task_3;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Homework {
    protected Homework() {
    }


    private static Scanner readerInterval = new Scanner(System.in);
    private static List<Integer> interval = new ArrayList<>();
    private static List<Integer> fibonacciNumbers = new ArrayList<>();


    public static void main(String[] args) {

        choosingInterval();

        filtrationOddAndEvenNumbers();

        summationOddAndEvenNumbers();

        calculateFibonacciSet();

        calculatePercentageOfOddAndEvenNumbers();


    }

    private static void choosingInterval() {
        int startNumber;
        int endNumber;


        System.out.println("Please enter interval");

        startNumber = readerInterval.nextInt();
        endNumber = readerInterval.nextInt();

        for (int i = startNumber; i <= endNumber; i++) {
            interval.add(i);
        }
    }

    private static void summationOddAndEvenNumbers() {
        System.out.println("Sum of odd interval:");
        System.out.println(interval.stream().
                filter(a -> a % 2 != 0)
                .mapToInt(Integer::intValue)
                .sum());
        System.out.println("Sum of even interval:");
        System.out.println(interval.stream()
                .filter(a -> a % 2 == 0)
                .mapToInt(Integer::intValue)
                .sum());
    }

    private static void filtrationOddAndEvenNumbers() {
        System.out.println("Odd interval:");
        interval.stream()
                .filter(a -> a % 2 != 0)
                .forEach(System.out::println);
        System.out.println("Even interval:");
        interval.stream()
                .filter(a -> a % 2 == 0)
                .sorted(Comparator.reverseOrder())
                .forEach(System.out::println);
    }

    private static void calculateFibonacciSet() {
        int firstNumber = fibonacciNumbers.size() - 2;
        int secondNumber = fibonacciNumbers.size() - 1;
        fibonacciNumbers.add(firstNumber);
        fibonacciNumbers.add(secondNumber);
        Integer sizeOfSet = -1;
        System.out.println("Please enter size of Fibonacci interval set:");
        do {
            Object obj = readerInterval.next();
            if (obj instanceof Number) {
                if ((int) obj < 0)
                    try {
                        throw new NegativeNumberException();
                    } catch (NegativeNumberException e) {
                        System.out.println("Please enter only positive numbers");
                    }
            } else if(obj instanceof String){

                    try {
                        throw new StringTypeToFibonachiNumbers();
                    } catch (StringTypeToFibonachiNumbers e) {
                        System.out.println("Enter numbers, not text");
                    }
            }
        } while (sizeOfSet < 0);


        int nextNumber;
        for (int i = 0; i < sizeOfSet - 2; i++) {
            nextNumber = firstNumber + secondNumber;
            fibonacciNumbers.add(nextNumber);
            firstNumber = secondNumber;
            secondNumber = nextNumber;

        }

    }

    private static void calculatePercentageOfOddAndEvenNumbers() {
        int count = 0;
        float percentageOfOdd;
        for (int i : fibonacciNumbers) {
            if (i % 2 != 0) {
                count++;
            }
        }
        percentageOfOdd = (100.0f * count) / fibonacciNumbers.size();
        System.out.println("Percentage of odd :" + percentageOfOdd + "%");
        System.out.println("Percentage of even :" + (100 - percentageOfOdd) + "%");
    }

}
