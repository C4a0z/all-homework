package my;

import java.util.Comparator;

public class GemsComparator implements Comparator<Gem> {
    @Override
    public int compare(Gem o1, Gem o2) {
        return Float.compare(o1.getWeight(),o2.getWeight());
    }
}
