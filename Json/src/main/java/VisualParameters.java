import java.io.Serializable;

public class VisualParameters implements Serializable {
    private String color;
    private String transparency;
    private String shape;
    private SizeOfGem sizeOfGem;
    private String clarity;

    public VisualParameters(String color, String transparency, String shape, SizeOfGem sizeOfGem, String clarity) {
        this.color = color;
        this.transparency = transparency;
        this.shape = shape;
        this.sizeOfGem = sizeOfGem;
        this.clarity = clarity;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTransparency() {
        return transparency;
    }

    public void setTransparency(String transparency) {
        this.transparency = transparency;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public SizeOfGem getSizeOfGem() {
        return sizeOfGem;
    }

    public void setSizeOfGem(SizeOfGem sizeOfGem) {
        this.sizeOfGem = sizeOfGem;
    }

    public String getClarity() {
        return clarity;
    }

    public void setClarity(String clarity) {
        this.clarity = clarity;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "color='" + color + '\'' +
                ", transparency='" + transparency + '\'' +
                ", shape='" + shape + '\'' +
                ", sizeOfGem=" + sizeOfGem +
                ", clarity='" + clarity + '\'' +
                '}';
    }
}
