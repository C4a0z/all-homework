package Task_2;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CountFibonNumberTest {

    @RepeatedTest(100)
    void makingCounting() {
        assertEquals((Integer) 610,makingCount(15));
    }

    static Integer makingCount(int requiredNumber) {
        int currentPosition = 2;
        int previousNumber = 0;
        int currentNumber = 1;
        int fibNumb = 0;

        while (currentPosition <= requiredNumber) {
            fibNumb = previousNumber + currentNumber;
            previousNumber = currentNumber;
            currentNumber = fibNumb;
            currentPosition++;
        }

        return fibNumb;
    }
}