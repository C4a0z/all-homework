import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.InputStream;

public class JSONSchemeValidator {

    private JSONObject rawSchema;
    private Schema schema;


    public void validateScheme() {
        try ( InputStream inputStream = getClass().getResourceAsStream("src/main/resources/GemsScheme.json")) {
            rawSchema = new JSONObject(new JSONTokener(inputStream));
            schema = SchemaLoader.load(rawSchema);
            schema.validate(new JSONObject("{\"hello\" : \"world\"}")); // throws a ValidationException if this object is invalid
        } catch (Exception e) {e.printStackTrace();
        }
    }
}
