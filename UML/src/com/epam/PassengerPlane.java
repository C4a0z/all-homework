package com.epam;

public class PassengerPlane extends AbstractPlane {
    private int numberOfSeats;

    public PassengerPlane(String name, float volumeOfFuel, float fuelConsumptionPerKilometer, int numberOfSeats) {
        super(name, volumeOfFuel, fuelConsumptionPerKilometer);
        this.numberOfSeats = numberOfSeats;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    @Override
    public String toString() {
        return "PassengerPlane{" +
                "numberOfSeats=" + numberOfSeats +
                '}';
    }
}
