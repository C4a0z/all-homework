package com.epam.Enums;

public class Menu {
    MenuItems startIcon = MenuItems.START;
    MenuItems saveIcon = MenuItems.SAVE;
    MenuItems loadIcon = MenuItems.LOAD;
    MenuItems settingsIcon = MenuItems.SETTINGS;
    MenuItems ExitIcon = MenuItems.EXIT;

    public static void main(String[] args) {
        Menu menu = new Menu();
        System.out.println("Please choose what you want to do");
        System.out.println("1. -" + menu.startIcon.getNameOfItems());
        System.out.println("2. -" + menu.saveIcon.getNameOfItems());
        System.out.println("3. -" + menu.loadIcon.getNameOfItems());
        System.out.println("4. -" + menu.settingsIcon.getNameOfItems());
        System.out.println("5. -" + menu.ExitIcon.getNameOfItems());
    }

}
