import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.everit.json.schema.ValidationException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class JsonReader {
    private File file = new File("src/main/resources/Gems.json");


    private Gson gson = new Gson();

    private com.google.gson.stream.JsonReader reader;
    private List<Gem> listOfGem = new ArrayList<>();

    public static void main(String[] args) {

        JsonReader user = new JsonReader();
        user.validateScheme();
        user.printList();
    }

    public void printList() {
        listOfGem.stream().forEach(System.out::println);
    }


    public void parsingFile() {
        {
            try {
                reader = new com.google.gson.stream.JsonReader(new FileReader(file));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        Type type = new TypeToken<List<Gem>>() {
        }.getType();
        listOfGem = gson.fromJson(reader, type);
    }


    public void validateScheme() throws ValidationException {
        JSONSchemeValidator validator = new JSONSchemeValidator();
        validator.validateScheme();
    }

    public JsonReader() {
        parsingFile();
    }

    public File getFile() {
        return file;
    }


    public Gson getGson() {
        return gson;
    }

    public com.google.gson.stream.JsonReader getReader() {
        return reader;
    }

    public List<Gem> getListOfGem() {
        return listOfGem;
    }
}
