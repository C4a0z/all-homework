ackage com.epam;

import java.util.Scanner;

public class Main {

    static Scanner scanner = new Scanner(System.in);
    static Company company = new Company();

    public static void main(String[] args) {
        start();
    }


    static void start() {
        System.out.println("Hello, what do you want to do");
        System.out.println("1 - show me total capacity in planes");
        System.out.println("2 - show me number of seats in planes");
        System.out.println("3 - sort planes for distance of fly");
        System.out.println("4 - find planes with set limit of consumption");
        switch (scanner.nextInt()) {
            case 1: {
                totalCapacity();
            }
            case 2: {
                numberOfSeats();
            }
            case 3: {
                showSortDistance();
            }
            case 4: {
                showPlanesWithLimitConsumption();
            }
        }

    }

    static void totalCapacity() {
        company.totalCapacity();
        start();
    }

    static void numberOfSeats() {
        company.totalNumberOfSeats();
        start();
    }

    static void showSortDistance() {
        company.sortingForDistanceOfFly();
        start();
    }

    static void showPlanesWithLimitConsumption() {
        company.findingPlanesWithSomeConsumption();
        start();
    }


}
