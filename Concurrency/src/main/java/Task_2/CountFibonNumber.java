package Task_2;

import java.time.Duration;
import java.time.LocalTime;


/**
 * @Warning: В дебагере проходит ровно,
 * но при запуске(1 к 10) тупо зависает и ничего не делает
 * почему так и не понял.
 */

public class CountFibonNumber {


    public CountFibonNumber(int requiredNumber) {
        this.requiredNumber = requiredNumber;
    }

    public static void main(String[] args) {


        System.out.println("/");
        CountFibonNumber countFibonNumber = new CountFibonNumber(15);
        System.out.println("/");
        LocalTime time = LocalTime.now();
        System.out.println("/");
        System.out.println(countFibonNumber.makingCounting());
        System.out.println("/");
        LocalTime time1 = LocalTime.now();
        System.out.println("/");
        Duration duration = Duration.between(time, time1);
        System.out.println("/");
        System.out.println("Time: " + duration.toMillis());


    }


    private int currentPosition = 2;
    private int requiredNumber;
    private int currentNumber = 1;
    private int previousNumber = 0;
    private int requiredFibonacciNumber;
    private static Object sync = new Object();


    private synchronized Integer makingCounting() {
        Thread t1 = new Thread(() -> {
            synchronized (sync) {
                for (; currentPosition <= requiredNumber; currentPosition++) {
                    try {
                        sync.wait();
                    } catch (Exception e) {
                    }
                    if (currentPosition <= requiredNumber) {
                        requiredFibonacciNumber = currentNumber + previousNumber;
                        previousNumber = currentNumber;
                        currentNumber = requiredFibonacciNumber;
                        System.out.println(currentNumber);
                        sync.notify();
                    } else {
                       break;
                    }

                }
            }
        });
        Thread t2 = new Thread(() -> {
            synchronized (sync) {
                for (; currentPosition <= requiredNumber; currentPosition++) {
                    sync.notify();
                    try {
                        sync.wait();
                    } catch (Exception e) {
                    }
                    if (currentPosition <= requiredNumber) {
                        requiredFibonacciNumber = currentNumber + previousNumber;
                        previousNumber = currentNumber;
                        currentNumber = requiredFibonacciNumber;
                        System.out.println(currentNumber);
                    } else {
                       break;
                    }

                }
            }
        });
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
        }
        return requiredFibonacciNumber;
    }


}



