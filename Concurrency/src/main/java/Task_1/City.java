package Task_1;

public class City {


    public static void main(String[] args) {

        Shop shop = new Shop();
        Factory factory = new Factory(shop);
        Consumer consumer = new Consumer(shop);

        Thread thread1 = new Thread(factory);
        Thread thread2 = new Thread(consumer);

        thread1.start();
        thread2.start();
    }

    static class Shop {
        private int product = 0;

        public synchronized int getProduct() {
            return product;
        }

        public synchronized void addProduct() {
            while (product >= 3) {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
            product++;
            System.out.println("В магазин привезли еще продуктов");
            System.out.println("Сейчас на складе: " + product);
            notify();
        }

        public synchronized void soldProduct() {
            while (product == 0) {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
            product--;
            System.out.println("Был куплен 1 товар");
            System.out.println("Осталось на скалде: " + product);
            notify();
        }

    }

    static class Factory implements Runnable {
        Shop shop;

        Factory(Shop shop) {
            this.shop = shop;
        }

        @Override
        public void run() {
            for (int i = 0; i < 12; i++) {
                shop.addProduct();
            }
        }
    }

    static class Consumer implements Runnable {

        Shop shop;

        public Consumer(Shop shop) {
            this.shop = shop;
        }

        @Override
        public void run() {

            for (int i = 0; i < 12; i++) {
                shop.soldProduct();
            }

        }
    }
}
