package com.epam.logical_masive.PriorityQueue;

import com.epam.logical_masive.Generic.shipwithdroid.Droid;

import java.util.PriorityQueue;

public class MyPriorityQueue <T extends Droid> {
    PriorityQueue<T> queueOfDroid = new PriorityQueue<>();

    public T getDroid(int positionOfDroid) {
        return queueOfDroid.peek();
    }

    public void setDroid(T arg) {
        this.queueOfDroid.add(arg);
    }
}
