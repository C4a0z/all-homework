package task1;

public class Task_1 {
    private final Integer a = 2;
    private final Integer b = 4;
    private final Integer c = 12;
    SumingTreeMumbers maxValue = (a, b, c) -> Integer.max(Integer.max(a, b), Integer.max(b, c));
    SumingTreeMumbers averageNumber = (a, b, c) -> (a + b + c) / 3;

    public static void main(String[] args) {
        Task_1 task_1 = new Task_1();
        System.out.println(task_1.maxValue.recievingThreeNumbers(task_1.a,task_1.b,task_1.c));
        System.out.println(task_1.averageNumber.recievingThreeNumbers(task_1.a,task_1.b,task_1.c));

    }
}
