package Task_6;

import java.io.File;
import java.io.IOException;

public class DirectoryContents {
    public static void main(String[] args) throws IOException {

        boolean canRead;
        File f = new File("."); // current directory

        File[] files = f.listFiles();
        for (File file : files) {
            final double bytes = file.length();
            final double kilobytes = bytes / 1024;
            final double megabytes = kilobytes / 1024;
            if (file.isDirectory()) {
                System.out.print("Папка:");

            } else {
                System.out.print("    Файл:");
            }
            System.out.println(file.getName());

            canRead = file.canRead();
            if (file.isDirectory()) {
                System.out.println("Свойства:");
                System.out.printf("Можно ли прочесть/изменить: %s/%s\n", file.canRead(), file.canWrite());
                System.out.printf("Скрытая папка: %s\n", file.isHidden());

            } else {
                System.out.println("    Свойства:");
                System.out.printf("Можно ли прочесть/изменить: %s/%s\n",
                        file.canRead(), file.canWrite());
                System.out.printf("Скрытый файл: %s\n",
                        file.isHidden());
                System.out.printf("Размер файла в килобайтах: %.2f\n",
                        kilobytes);
            }
        }

    }
}
