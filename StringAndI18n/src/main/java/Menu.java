import Task_1.StringUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Menu {

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    Locale locale;
    ResourceBundle bundle;
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.showMenu();


    }

    public Menu() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("Menu", locale);
        settingMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::testStringUtils);
        methodsMenu.put("2", this::internalisationMenuRussian);
        methodsMenu.put("3", this::internalizationMenuEnglish);
        methodsMenu.put("4", this::splitingSentence);
        methodsMenu.put("5", this::checkSentence);


    }

    private void testStringUtils() {
        StringUtils utils = new StringUtils();
        utils.addNewParameter(11)
                .addNewParameter("Hello")
                .addNewParameter(1 + "sad");
        System.out.println(utils.concat());
    }

    private void internalisationMenuRussian() {
        locale = new Locale("ru");
        bundle = ResourceBundle.getBundle("Menu", locale);
        settingMenu();
        showMenu();
    }

    private void internalizationMenuEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("Menu", locale);
        settingMenu();
        showMenu();
    }

    private void settingMenu() {
        menu = new LinkedHashMap<>();

        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("Q", bundle.getString("Q"));
    }

    private void showMenu() {
        System.out.println("What you want to choose?");

        String keyMenu;
        do {
            for (String key : menu.keySet()) {
                System.out.println(menu.get(key));
            }

            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void splitingSentence() {
        String sentence = "I think the Expanse show is very interesting";
        String[] firstPart = sentence.split(" the ");
        Arrays.stream(firstPart).forEach(System.out::println);

    }

    private void checkSentence() {
        StringBuilder sb = new StringBuilder();
        String sentence = "It`s my testing sentence.";
        Pattern pattern = Pattern.compile("^[A-Z].+\\.");
        Matcher matcher = pattern.matcher(sentence);

        System.out.println(matcher.find());
    }
}
