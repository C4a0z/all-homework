<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

    <xsl:template match="/">
        <html>
            <head>
                <style type="text/css">
                    table {
                    border: 1px ;
                    }

                    td {
                    border: 1px ;
                    background-color: yellow;
                    color: red;
                    text-align:right;
                    }

                    th {
                    background-color: #2E9AFE;
                    color: white;
                    }
                </style>
            </head>

            <body style="font-family: Arial; font-size: 12pt; background-color: #EEE">
                <div>
                  <tr>
                          <th style="width:250px" borderstyle="solid">Name</th>
                          <th style="width:350px">Preciousness</th>
                          <th style="width:250px">Origin</th>
                          <th style="width:250px">Color</th>
                          <th style="width:250px">Transparency</th>
                          <th style="width:250px">Shape</th>
                          <th style="width:250px">Height</th>
                          <th style="width:250px">Width</th>
                          <th style="width:250px">Clarity</th>
                          <th style="width:250px">Weight</th>
                          <th style="width:250px">Treatment</th>

                      </tr>

                    <xsl:for-each select="Gems/gems/gem">

                        <tr>
                            <td>
                                <xsl:value-of select="name"/>
                            </td>
                            <td>
                                <xsl:value-of select="gem/preciousness"/>
                            </td>

                            <td>
                                <xsl:value-of select="origin"/>
                            </td>
                            <td>
                                <xsl:value-of select="color"/>
                            </td>

                            <td>
                                <xsl:value-of select="shape"/>
                            </td>

                            <td>
                                <xsl:value-of select="clarity"/>
                            </td>

                            <td>
                                <xsl:value-of select="weight"/>
                            </td>

                            <td>
                                <xsl:value-of select="treatment"/>
                            </td>

                        </tr>

                    </xsl:for-each>


                </div>

            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>