package com.epam.logical_masive;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ExerciseA {
    static int[] mass1 = {1, 2, 4, 5};
    static int[] mass2 = {2, 3, 6, 5};
    static int[] mass3 = new int[mass1.length + mass2.length];

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("1 - add numbers from both array");
        System.out.println("2 - add unique numbers");

        switch (scanner.nextInt()) {
            case 1: {

                addingNumbers();
                break;
            }
            case 2: {

                addingUniqueNumbers();
                break;

            }
        }
    }

    static void addingNumbers() {

        int elementInMass1 = mass1.length - 1;
        int elementInMass2 = mass2.length - 1;
        int counter = 0;
        while (true) {

            if (elementInMass1 >= 0) mass3[counter] = mass1[mass1.length - 1 - elementInMass1];
            counter++;
            if (elementInMass2 >= 0) mass3[counter] = mass2[mass2.length - 1 - elementInMass2];
            counter++;
            elementInMass1--;
            elementInMass2--;
            if (elementInMass1 < 0 && elementInMass2 < 0) {
                break;
            }
        }
        System.out.println("Showing array");
        for (int i : mass3) {
            System.out.println(i);
        }


    }


    static void addingUniqueNumbers() {


        boolean checkBoolean;
        int counter = 0;
        for (int i : mass1) {
            checkBoolean = false;
            for (int k : mass3) {
                if (i == k) {
                    checkBoolean = true;
                    break;
                }
            }
            for (int k : mass2) {
                if (i == k) {
                    checkBoolean = true;
                    break;
                }
            }
            if (checkBoolean == false) {
                mass3[counter] = i;
                counter++;
            }
        }
        for (int i : mass2) {
            checkBoolean = false;
            for (int k : mass3) {
                if (i == k) {
                    checkBoolean = true;
                    break;
                }
            }
            for (int j : mass1) {
                if (j == i) {
                    checkBoolean = true;
                    break;
                }
            }


            if (checkBoolean == false) {
                mass3[counter] = i;
                counter++;
            }
        }
        System.out.println("Showing array");
        for (int i : mass3) {
            System.out.println(i);
        }


    }
}








