package Task_4;

import java.io.BufferedInputStream;
import java.io.FileInputStream;

//не очень понял смысл задания. Сделал как понял
public class ReadingTextFromConsole {
    public static void main(String[] args) {
        readTextFromFile();

    }

    static void readTextFromFile() {
        try (FileInputStream input = new FileInputStream("file.txt");
             BufferedInputStream bufferedInput = new BufferedInputStream(input)
        ) {

            int data = bufferedInput.read();
            while (data != -1) {
                System.out.print((char) data);
                data = bufferedInput.read();
            }
        } catch (Exception e) {
        }
    }
}
