package Task_3;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AirShip implements Serializable {

   private List<People> listOfPassengers = new ArrayList<>();


    public AirShip() {
        fillingList();
    }

    void fillingList() {
        for (int i = 0; i < 6000000; i++) {
            listOfPassengers.add(
                    new People("Passenger " + i, 10 + i));
        }
    }

    public List<People> getListOfPassengers() {
        return listOfPassengers;
    }
}
