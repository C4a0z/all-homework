package com.epam.logical_masive.ExcersiseD;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MainRoom {
    List<Room> doorForAnotherRoom = new ArrayList<>();
    Hero mainHero = new Hero();


    public static void main(String[] args) {
        MainRoom room = new MainRoom();
        room.printingDoorInRoom();
        System.out.println("How many rooms can kill");
        System.out.println(room.inHowManyRoomsHeroCanDie(room.doorForAnotherRoom.size() - 1, 0));
        System.out.println("Order of door to win");
        room.orderOfOpeningDoors();
    }

    private void generationRoom() {
        for (int i = 0; i < 10; i++) {
            doorForAnotherRoom.add(new Room(i + 1));
        }
    }

    void printingDoorInRoom() {
        doorForAnotherRoom.stream().forEach(a -> System.out.println(a.toString()));
    }

    public MainRoom() {
        generationRoom();
    }

    int inHowManyRoomsHeroCanDie(int numberOfRoom, int howManyRoomsCanKill) {
        if (numberOfRoom != 0) {
            if (mainHero.getPower() + doorForAnotherRoom
                    .get(numberOfRoom)
                    .getTypeOfAction()
                    .getWhatCanDoWithPower() < 0) {
                return inHowManyRoomsHeroCanDie(--numberOfRoom, ++howManyRoomsCanKill);

            } else {
                return inHowManyRoomsHeroCanDie(--numberOfRoom, howManyRoomsCanKill);
            }
        } else {
            return howManyRoomsCanKill;
        }
    }

    void orderOfOpeningDoors() {
        List<Room> copyOfMainRoom;
        copyOfMainRoom = doorForAnotherRoom;
        var listOfRooms = new ArrayList<Integer>();
        try {
            while ((copyOfMainRoom.size() - 1) != 0) {
                if (doorForAnotherRoom.stream().filter(a -> a.getTypeOfAction()
                        .getName()
                        .equals("Monster"))
                        .anyMatch(a -> a.getTypeOfAction()
                                .getWhatCanDoWithPower() + mainHero.getPower() >= 0)) {
                    Room room = doorForAnotherRoom.stream()
                            .filter(a -> a.getTypeOfAction()
                                    .getName()
                                    .equals("Monster"))
                            .filter(a -> a.getTypeOfAction()
                                    .getWhatCanDoWithPower() + mainHero.getPower() >= 0)
                            .collect(Collectors.toList()).get(0);
                    listOfRooms.add(room.getNumberOfRoom());
                    mainHero.setPower(mainHero.getPower() + room.getTypeOfAction().getWhatCanDoWithPower());
                    for (int i = 0; i < copyOfMainRoom.size() - 1; i++) {
                        if (copyOfMainRoom.get(i).getTypeOfAction().getWhatCanDoWithPower().equals(room.getTypeOfAction().getWhatCanDoWithPower())) {
                            copyOfMainRoom.remove(i);
                            break;
                        }
                    }
                } else {
                    Room room = doorForAnotherRoom.stream()
                            .filter(a -> a.getTypeOfAction()
                                    .getName()
                                    .equals("Artifact"))
                            .max((Room a, Room b) -> a.getTypeOfAction().getWhatCanDoWithPower()).get();
                    mainHero.setPower(mainHero.getPower() + room.getTypeOfAction().getWhatCanDoWithPower());
                    listOfRooms.add(room.getNumberOfRoom());
                    for (int i = 0; i < copyOfMainRoom.size() - 1; i++) {
                        if (copyOfMainRoom.get(i).getTypeOfAction().getWhatCanDoWithPower().equals(room.getTypeOfAction().getWhatCanDoWithPower())) {
                            copyOfMainRoom.remove(i);
                            break;
                        }
                    }
                }
            }

            listOfRooms.stream().forEach(System.out::println);
        } catch (Exception e) {
            System.out.println("К сожалеию пройти игру невозможно");
        }

    }
}
