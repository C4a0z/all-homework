package task3;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GeneratorOfLists {

    List<Integer> generateList(Integer numberOfValues) {
        List<Integer> list = Stream.generate(() -> ((int) (Math.random() * 250)))
                .limit(numberOfValues)
                .collect(Collectors.toList());

        return list;
    }

    public static void main(String[] args) {
        List<Integer> mainList = new GeneratorOfLists().generateList(40);

        int minValue = mainList.stream().min(Integer::compareTo).get();
        int maxValue = mainList.stream().max(Integer::compareTo).get();
        int sumOfValue = mainList.stream().mapToInt(Integer::intValue).sum();
        double averageOfValue = mainList.stream().mapToInt(Integer::intValue).average().getAsDouble();
        int sumReduce = mainList.stream().reduce(Integer::sum).get();

        int numberOfValuesThatBiggerThanAverage =(int) mainList.stream()
                .filter(a -> a > averageOfValue).count();
    }

}
