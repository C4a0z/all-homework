package Task_5;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class SleepingThread {
    static int timeForFirstTread = (int) (Math.random() * 5);
    static int timeForSecondTread = (int) (Math.random() * 5);
    static ScheduledExecutorService service = Executors.newScheduledThreadPool(2);

    public static void main(String[] args) {
        Runnable runnableTread1 = () -> {
            System.out.println("1 поток спал :" + timeForFirstTread + " секунд");
            System.out.println("Проснулся 1 поток");
        };
        Runnable runnableTread2 = () -> {
            System.out.println("Проснулся 2 поток");
            System.out.println("2 поток спал :" + timeForFirstTread + " секунд");
        };
        Runnable runnableTread3 = () -> {
            System.out.println("Все потоки завершили работу");
            service.shutdown();
        };


        service.scheduleWithFixedDelay(runnableTread1, timeForFirstTread, 2, TimeUnit.SECONDS);
        service.scheduleWithFixedDelay(runnableTread2, timeForSecondTread, 2, TimeUnit.SECONDS);

        service.scheduleWithFixedDelay(runnableTread3, 20, 2, TimeUnit.SECONDS);


    }
}
