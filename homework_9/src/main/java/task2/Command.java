package task2;

@FunctionalInterface
public interface Command {
    void receivingStringArgument(String arg);
}