import java.io.Serializable;

public class SizeOfGem implements Serializable {
    private float height;
    private float width;
    private float diameter;

    public SizeOfGem(float height, float width, float diameter) {
        this.height = height;
        this.width = width;
        this.diameter = diameter;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getDiameter() {
        return diameter;
    }

    public void setDiameter(float diameter) {
        this.diameter = diameter;
    }

    @Override
    public String toString() {
        return "SizeOfGem{" +
                 height +
                " x " + width +
                " x " + diameter +
                '}';
    }
}
