package Task_2;

import java.io.Serializable;

public class People implements Serializable {
    private String name;
    private Integer age;
    private transient Integer money;

    public People(String name, Integer age) {
        this.name = name;
        this.age = age;
        this.money = 5000;
    }

    @Override
    public String toString() {
        return "Task_2.People{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
