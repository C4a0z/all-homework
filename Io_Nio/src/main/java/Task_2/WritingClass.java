package Task_2;

import java.io.*;

public class WritingClass<T> {

    public static void main(String[] args) {
        AirShip airShip = new AirShip();
        WritingClass<AirShip> writingClass = new WritingClass<>();
        writingClass.writeObjToFile(airShip);

        AirShip airShip1 = writingClass.readFileToObj();

        airShip1.getListOfPassengers().stream().forEach(System.out::println);
    }

   protected void writeObjToFile(T object) {
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(
                    new FileOutputStream("Task_2.AirShip.dat"));
            out.writeObject(object);
        } catch (Exception e) {
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

   protected T readFileToObj() {
        ObjectInputStream ois = null;
        T obj = null;
        try {
            ois = new ObjectInputStream(new FileInputStream("Task_2.AirShip.dat"));
            obj = (T) ois.readObject();
            ois.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
        }

        return obj;
    }

}
