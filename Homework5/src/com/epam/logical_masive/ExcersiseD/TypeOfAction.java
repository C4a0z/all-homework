package com.epam.logical_masive.ExcersiseD;

public class TypeOfAction {
   private String name;
  private  Integer whatCanDoWithPower;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getWhatCanDoWithPower() {
        return whatCanDoWithPower;
    }

    public void setWhatCanDoWithPower(Integer whatCanDoWithPower) {
        this.whatCanDoWithPower = whatCanDoWithPower;
    }

    @Override
    public String toString() {
        return "TypeOfAction{" +
                "name='" + name + '\'' +
                ", whatCanDoWithPower=" + whatCanDoWithPower +
                '}';
    }
}
