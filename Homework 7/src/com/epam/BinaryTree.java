package com.epam;

public class BinaryTree<V> {
    private Node root;

    public Node get(int id) {
        Node current = root;
        while (current.iData != id) {
            if (id < current.iData) {
                current = current.leftChild;
            } else {
                current = current.rightChild;
            }
            if (current == null) return null;
        }
        return current;
    }

    public void put(int id, V dd) {
        Node newNode = new Node();
        newNode.iData = id;
        newNode.data = dd;
        if (root == null) {
            root = newNode;
        } else {
            Node current = root;
            Node parent;
            while (true) {
                parent = current;
                if (id < current.iData) {
                    current = current.leftChild;
                    if (current == null) {
                        parent.leftChild = newNode;
                        return;
                    }
                } else {
                    current = current.rightChild;
                    if (current == null) {
                        parent.rightChild = newNode;
                        return;
                    }
                }
            }
        }
    }

    public boolean remove(int id) {
        Node current = root;
        Node parent = root;
        boolean isLeftChild = true;
        while (current.iData != id) {
            parent = current;
            if (id < current.iData) {
                isLeftChild = true;
                current = current.leftChild;
            } else {
                isLeftChild = false;
                current = current.rightChild;
            }
            if (current == null) {
                return false;
            }
            if (current.rightChild == null) {
                if (current == root) {
                    root = current.leftChild;
                } else {
                    if (isLeftChild) {
                        parent.leftChild = current.leftChild;
                    } else {
                        parent.rightChild = current.leftChild;
                    }
                }
            } else {
                if (current.leftChild == null) {
                    if (current == root)
                        root = current.rightChild;
                } else if (isLeftChild) {
                    parent.leftChild = current.rightChild;
                } else {
                    parent.rightChild = current.rightChild;
                }
            }


        }
        return false;
    }

    public void print(int id) {
        Node current = root;
        while (current.iData != id) {
            if (id < current.iData) {
                current = current.leftChild;
            } else {
                current = current.rightChild;
            }

        }
        System.out.println(current.data);
    }
}
