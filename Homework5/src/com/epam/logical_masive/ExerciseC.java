package com.epam.logical_masive;

import java.util.Arrays;

public class ExerciseC {
    Integer[] arrayOfNumbers = {1, 2, 1, 1, 1, 24, 35, 12, 12, 12, 12, 12, 3, 1, 35, 1512, 125};

    private void deletingCopies() {
        Integer currentNumber = arrayOfNumbers[0];
        for (int i = 1; i < arrayOfNumbers.length; i++) {
            if (!currentNumber.equals(arrayOfNumbers[i])) {
                currentNumber = arrayOfNumbers[i];
            } else {
                remove(i);
                i--;
            }

        }
    }

    private void remove(int index) {
        if (index >= 0 && index < arrayOfNumbers.length) {
            Integer[] copy = new Integer[arrayOfNumbers.length - 1];
            System.arraycopy(arrayOfNumbers, 0, copy, 0, index);
            System.arraycopy(arrayOfNumbers, index + 1, copy, index, arrayOfNumbers.length - index - 1);
            arrayOfNumbers = copy;
        }

    }

    public static void main(String[] args) {
        ExerciseC exerciseC = new ExerciseC();
        exerciseC.deletingCopies();
        System.out.println(Arrays.toString(exerciseC.arrayOfNumbers));
    }
}
