package com.epam.logical_masive.ExcersiseD;

public class Hero {
   private String name;
   private int power = 25;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }


}
