package my.Parser;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DomPars {
    private static Document getDocument() throws Exception {
        try {
            DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
            f.setValidating(false);
            DocumentBuilder builder = f.newDocumentBuilder();
            return builder.parse(new File("C:\\MyProj\\all-homework\\all-homework\\XML\\src\\main\\resources\\xml\\Gems.xml"));
        } catch (Exception exception) {
            String message = "XML parsing error!";
            throw new Exception(message);
        }
    }
    private static void showDocument(Document doc){
        StringBuffer content = new StringBuffer();
        Node node = doc.getChildNodes().item(0);
        ApplicationNode appNode = new ApplicationNode(node);
    }
    public static class ApplicationNode {

        Node node;

        public ApplicationNode(Node node) {
            this.node = node;
        }

        public List<ClassNode> getClasses() {
            ArrayList<ClassNode> classes = new ArrayList<ClassNode>();

            NodeList classNodes = node.getChildNodes();

            for (int i = 0; i < classNodes.getLength(); i++) {
                Node node = classNodes.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {


                    ClassNode classNode = new ClassNode(node);
                    classes.add(classNode);
                }
            }

            return classes;
        }

    }
    public static class ClassNode {

        Node node;


        public ClassNode(Node node) {
            this.node = node;
        }


        public List<MethodNode> getMethods() {
            ArrayList<MethodNode> methods = new ArrayList<MethodNode>();


            NodeList methodNodes = node.getChildNodes();

            for (int i = 0; i < methodNodes.getLength(); i++) {
                node = methodNodes.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {


                    MethodNode methodNode = new MethodNode(node);
                    methods.add(methodNode);
                }
            }

            return methods;
        }


        public String getName() {


            NamedNodeMap attributes = node.getAttributes();


            Node nameAttrib = attributes.getNamedItem("name");


            return nameAttrib.getNodeValue();
        }
    }


    public static class MethodNode {

        Node node;


        public MethodNode(Node node) {
            this.node = node;
        }


        public String getName() {


            NamedNodeMap attributes = node.getAttributes();

            Node nameAttrib = attributes.getNamedItem("name");


            return nameAttrib.getNodeValue();
        }

    }

    public static void main(String[] args) {
        try {
            Document doc = getDocument();
            showDocument(doc);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

