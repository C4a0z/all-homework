package com.epam;

public abstract class AbstractPlane {
    private String name;
    private float volumeOfFuel;
    private float fuelConsumptionPerKilometer;
    private Integer distanceOfFly;

    public AbstractPlane(String name, float volumeOfFuel, float fuelConsumptionPerKilometer) {
        this.name = name;
        this.volumeOfFuel = volumeOfFuel;
        this.fuelConsumptionPerKilometer = fuelConsumptionPerKilometer;
        this.distanceOfFly =(int) (volumeOfFuel / fuelConsumptionPerKilometer);
    }

    public String getName() {
        return name;
    }

    public float getVolumeOfFuel() {
        return volumeOfFuel;
    }

    public float getFuelConsumptionPerKilometer() {
        return fuelConsumptionPerKilometer;
    }

    public Integer getDistanceOfFly() {
        return distanceOfFly;
    }

    @Override
    public String toString() {
        return "AbstractPlane{" +
                "name='" + name + '\'' +
                ", volumeOfFuel=" + volumeOfFuel +
                ", fuelConsumptionPerKilometer=" + fuelConsumptionPerKilometer +
                ", distanceOfFly=" + distanceOfFly +
                '}';
    }



}
